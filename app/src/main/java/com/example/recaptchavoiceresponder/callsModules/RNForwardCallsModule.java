package com.example.recaptchavoiceresponder.callsModules;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

public class RNForwardCallsModule {

    private Context context;

    @RequiresApi(api = Build.VERSION_CODES.M)
    public RNForwardCallsModule (Context context) {
        this.context = context;
        if (context.checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context, PackageManager.PERMISSION_GRANTED + "", Toast.LENGTH_SHORT).show();
            Toast.makeText(context, context.checkSelfPermission(Manifest.permission.CALL_PHONE) + "", Toast.LENGTH_SHORT).show();
            Toast.makeText(context, "CALL_PHONE Permission is missing", Toast.LENGTH_SHORT).show();
            Toast.makeText(context, "Please add permissions", Toast.LENGTH_SHORT).show();
            // TODO: Consider calling
            //    Activity#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for Activity#requestPermissions for more details.
        }
    }

    /**
     * Forward all calls unconditionally to the number 123456789
     * @param phoneNumber
     */
    public void unconditionalForwarding(String phoneNumber) {
        String uri = "tel:**21*" + Uri.encode(phoneNumber + "#");
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uri));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        try {
            context.startActivity(intent);
        } catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    /**
     * All calls forwarding activated to the number phoneNumber, place code
     * @param phoneNumber
     */
    public void customCodeForwarding(String code,String phoneNumber) {
        String uri = "tel:" + code + Uri.encode(phoneNumber + "#");
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uri));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //You already have permission
        try {
            context.startActivity(intent);
        } catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    
    public void forwardCallsIfBusy(String phoneNumber) {
        String uri = "tel:**67*" + Uri.encode(phoneNumber+"#");
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uri));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //You already have permission
        try {
            context.startActivity(intent);
        } catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    
    public void forwardCallsIfNotAnswered (String phoneNumber) {
        String uri = "tel:*61*" + Uri.encode(phoneNumber+"#");
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uri));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //You already have permission
        try {
            context.startActivity(intent);
        } catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    
    public void forwardCallsIfOutOfReach (String phoneNumber) {
        String uri = "tel:*62*" + Uri.encode(phoneNumber+"#");
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uri));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //You already have permission
        try {
            context.startActivity(intent);
        } catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    /**
     * All conditional forwarding activated to the number phoneNumber
     * @param phoneNumber
     */
    public void allConditionalForwarding (String phoneNumber) {
        String uri = "tel:*002*" + Uri.encode(phoneNumber+"#");
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uri));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        //You already have permission
        try {
            context.startActivity(intent);
        } catch(SecurityException e) {
            e.printStackTrace();
        }
    }
    
    public void cancelUnconditional() {
        String uri = "tel:" + Uri.encode("##21#");
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uri));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //You already have permission
        try {
            context.startActivity(intent);
        } catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    public void cancelIfBusy() {
        String uri = "tel:" + Uri.encode("##67#");
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uri));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //You already have permission
        try {
            context.startActivity(intent);
        } catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    public void cancelIfNotAnswered() {
        String uri = "tel:" + Uri.encode("##61#");
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uri));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //You already have permission
        try {
            context.startActivity(intent);
        } catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    public void cancelIfOutOfReach() {
        String uri = "tel:" + Uri.encode("##62#");
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uri));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //You already have permission
        try {
            context.startActivity(intent);
        } catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    public void cancelAllConditional() {
        String uri = "tel:" + Uri.encode("##002#");
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uri));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //You already have permission
        try {
            context.startActivity(intent);
        } catch(SecurityException e) {
            e.printStackTrace();
        }
    }

    public void cancelCustomForward(String code) {
        String uri = "tel:" + Uri.encode(code);
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse(uri));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        //You already have permission
        try {
            context.startActivity(intent);
        } catch(SecurityException e) {
            e.printStackTrace();
        }
    }
    
}
