package com.example.recaptchavoiceresponder.ui.home

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.recaptchavoiceresponder.R
import com.example.recaptchavoiceresponder.callsModules.RNForwardCallsModule

class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    private var onOff = false


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_home, container, false)
        val btnPower: Button = root.findViewById(R.id.btn_power)
        btnPower.setOnClickListener { power(btnPower) }
        //val textView: TextView = root.findViewById(R.id.text_home)
        homeViewModel.text.observe(this, Observer {
            //textView.text = it
        })
        return root
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun power(btnPower:Button) {
        if (onOff) {
            btnPower.setBackgroundResource(R.drawable.power_off)
            val callForwarder = RNForwardCallsModule(context)
            callForwarder.cancelUnconditional()
            onOff = false
        }
        else {
            btnPower.setBackgroundResource(R.drawable.power)
            val callForwarder = RNForwardCallsModule(context)
            callForwarder.unconditionalForwarding("2054305921")
            onOff = true
        }
    }
}