package com.example.recaptchavoiceresponder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.example.recaptchavoiceresponder.callsModules.RNCallKeepModule;

public class PhoneStateReceiver extends BroadcastReceiver {

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onReceive(Context context, Intent intent) {

        try {
            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);
            String incomingNumber = intent.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER);
            assert state != null;
            if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {
                RNCallKeepModule rnCallKeepModule = new RNCallKeepModule(context);
                Log.d("displayIncomingCall","displayIncomingCall");
                rnCallKeepModule.displayIncomingCall("uuid" +incomingNumber, incomingNumber, "sys" );
                rnCallKeepModule.answerIncomingCall("uuid" +incomingNumber);
                rnCallKeepModule.setOnHold("uuid" +incomingNumber, true);
                ApiTask backgroundTask = new ApiTask(context);
                //execute the task
                //passes the paras to the backgroundTask (param[0],param[1],param[2])
                backgroundTask.execute("captcherinit", incomingNumber, "99999999999");
            }
            if ((state.equals(TelephonyManager.EXTRA_STATE_OFFHOOK))) {
                Toast.makeText(context, "Received State", Toast.LENGTH_SHORT).show();
            }
            if (state.equals(TelephonyManager.EXTRA_STATE_IDLE)) {
                Toast.makeText(context, "Idle State", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
