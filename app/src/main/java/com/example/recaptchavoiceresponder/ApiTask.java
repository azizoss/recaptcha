package com.example.recaptchavoiceresponder;

import android.app.AlertDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;

public class ApiTask extends AsyncTask<String, String, String> {

    Context context;
    private String resp;

    ApiTask(Context ctx) {
        this.context = ctx;
    }

    @Override
    protected String doInBackground(String... params) {

        String urlApi = "http://admin.canmorpro.com/";
        String action = params[0];
        String actionUrl = "";
        InputStream is;
        String response = "";


        switch (action) {
            case "captcherinit":
                actionUrl = "captcherinit";
                break;
            default:
                display("Login Failed...","Something weird happened :(.");
                return null;
        }

        try {
            String caller = params[1];
            String receiver = params[2];
            URL url = new URL(urlApi+actionUrl);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");

            //Sets headers: Content-Type, Authorization
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.setRequestProperty("Accept", "application/json");
            //httpURLConnection.setRequestProperty("Authorization", "Token fab11c9b6bd4215a989c5bf57eb678");

            //Add POST data in JSON format
            JSONObject jsonParam = new JSONObject();
            try {
                jsonParam.put("caller", caller);
                jsonParam.put("receiver", receiver);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            //Create a writer object and make the request
            OutputStreamWriter outputStream = new OutputStreamWriter(httpURLConnection.getOutputStream());
            outputStream.write(jsonParam.toString());
            outputStream.flush();
            outputStream.close();

            if(httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK){
                is = httpURLConnection.getInputStream();
            } else {
                is = httpURLConnection.getErrorStream();
            }

            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                response = sb.toString();
                Log.e("JSON", response);
            } catch (Exception e) {
                Log.e("Buffer Error", "Error converting result " + e.toString());
            }

            return response;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(String result) {
        String init_id = "";
        String forward = "";
        try {
            JSONObject resJsonObject = new JSONObject(result);
            init_id = resJsonObject.getString("init_id") ;
            forward = resJsonObject.getString("forward") ;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //Print the response code as toast popup
        Toast.makeText(context, "Response : init_id = " + init_id + " forward = " + forward,
                Toast.LENGTH_LONG).show();
    }


    @Override
    protected void onPreExecute() {
    }


    @Override
    protected void onProgressUpdate(String... text) {
    }

    private void display(String title, String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.show();
    }
}